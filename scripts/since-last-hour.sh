# 1 HOUR DELAY
DELAY=0.04166666667
find / -not \( -path /proc -prune \) -not \( -path /usr -prune \) -not \( -path /sys -prune \) -not \( -path /cgroup -prune \)  -mtime -$DELAY -print 2>&1 | grep -v "Permission denied"
