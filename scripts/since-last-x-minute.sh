
# 1 MINUTE unit
PERMINUTE=0.0007

# 1 MIN. default value or take from $1 param
MIN=${1:-1}
MTIME=`echo "$PERMINUTE * $1" | bc`


find / -not \( -path /proc -prune \) -not \( -path /usr -prune \) -not \( -path /sys -prune \) -not \( -path /cgroup -prune \)  -mtime -$MTIME -print 2>&1 | grep -v "Permission denied"
